<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
* {
    text-align: center;
}

fieldset {
    width: 470px;
    margin: 12%;
    margin-left: 32%;
    border: 1px solid #1E90FF;
}

.username {
    background-color: #5b9bd5;
    color: azure;
    padding: 10px 20px;
    margin-right: 25px;
    width: 100px;
    text-align: center;
    border: 1px solid #1E90FF
}

.text {
    border: 1px solid #1E90FF;
    padding: 10px;
    width: 278px;
}

.apartment {
    margin-top: 20px;

}

.department {
    width: 300px;
    border: 1px solid #1E90FF;
}

.button {
    width: 100px;
    margin-top: 30px;
    height: 30px;
    background-color: #008000;
    border: 1px solid #1E90FF;
    border-radius: 5px;
    color: white;
}

.button:hover {
    opacity: 0.8;
}

.style {
    display: flex;
}

.text {
    border: 1px solid #1E90FF;
}

.notnull {
    color: red;
}
</style>

<body>
    <?php
    session_start();
    $error = array();
    $data = array();
    $_SESSION = $_POST;
    $allowed_exttension = array('gif', 'png', 'jpg','jpeg');

    if (!empty($_POST['button'])) {
        //lấy dữ liệu
        $_SESSION['user_name'] = isset($_POST['user_name']) ? $_POST['user_name'] : '';
        $_SESSION['gender'] = isset($_POST['gender']) ? $_POST['gender'] : '';
        $_SESSION['department'] = isset($_POST['department']) ? $_POST['department'] : '--Department--';
        $_SESSION['date'] = isset($_POST['date']) ? $_POST['date'] : '';

        
        //KT du lieu
        if (empty($_SESSION['user_name'])) {
            $error['user_name'] = 'Hãy nhập tên';
        }

        if (empty($_SESSION['gender'])) {
            $error['gender'] = 'Hãy chọn giới tính';
        }

        if (empty($_SESSION['department']) || $_SESSION['department'] == '--Department--') {
            $error['department'] = 'Hãy chọn phân khoa ';
        }

        if (empty($_SESSION['date'])) {
            $error['date'] = 'Hãy nhập ngày sinh';
        }
        //Thư mục bạn sẽ lưu file upload
        $target_dir    = "uploads/";
        if (!file_exists($target_dir)) {
            mkdir($target_dir);
        }
        $filename = $_FILES["pic"]["tmp_name"];
        $prod = "pic";
        $extension = pathinfo(basename($_FILES["pic"]["name"]), PATHINFO_EXTENSION); // jpg
        $newfilename = $prod . "_" . $date . "." . $extension;
        //Vị trí file lưu tạm trong server (file sẽ lưu trong uploads, với tên giống tên ban đầu)
        $target_file   = $target_dir . basename($newfilename);

        // Lưu dữ liệu
        if (empty($error)) {
            move_uploaded_file($_FILES["pic"]["tmp_name"], $target_file);
            $_SESSION['pic'] = $target_file;
            header("Location: ./regiter_submit.php ");
        }


     }

    ?>
    <form method="post" action="">
        <fieldset class="day06-forn">
            <div class="forn">
                <?php echo isset($error['user_name']) ? $error['user_name'] : ''; ?> <br />
                <?php echo isset($error['gender']) ? $error['gender'] : ''; ?> <br />
                <?php echo isset($error['department']) ? $error['department'] : ''; ?> <br />
                <?php echo isset($error['date']) ? $error['date'] : ''; ?> <br />


                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Họ và tên <sup class="notnull">*</sup>
                            </label>
                        </div>
                        <input type="text" class="text" id="user_name" name="user_name">
                    </div>
                </div>
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Giới tính<sup class="notnull">*</sup>
                            </label>
                        </div>
                        <?php
                        $gender = array("Nam", "Nữ");
                        $keys = array_keys($gender);
                        for ($i = 0; $i <= count($gender) - 1; $i++) { ?>
                        <input type="radio" , name="gender" id="gender" checked="<?php echo "checked"; 
                            ?>" value="<?php echo $gender[$i]; ?>"> <?php echo $gender[$i]; ?>

                        <?php
                        } ?>
                    </div>
                </div>
                <div>
                </div>
            </div>
            </div>
            <div class="apartment">
                <div class="style">
                    <div class="username">
                        <label>
                            Phân khoa <sup class="notnull">*</sup>
                        </label>
                    </div>
                    <select class="department" name="department" id="department">
                        <?php $department = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu'); 
                             foreach ($department as $key => $value) {  
                                echo " <option "; 
                                echo isset($_POST['Faculty']) && $_POST['Faculty'] == $key ? " selected " : ""; 
                                echo " value='" . $key . "'>" . $value . "</option> "; 
                            }
                         ?>
                    </select>
                </div>
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Sinh nhật <sup class="notnull">*</sup>
                            </label>
                        </div>
                        <input type="date" name="date" id="date" class="date">
                    </div>
                </div>
                <div>
                    <div class="apartment">
                        <div class="style">
                            <div class="username">
                                <label>
                                    Địa chỉ
                                </label>
                            </div>
                            <input type="text" class="text" id="diachi" name="diachi">
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="apartment">
                    <div class="style">
                        <div class="username">
                            <label>
                                Hình ảnh
                            </label>
                        </div>
                        <input type="file" class="pic" id="pic" name="pic">
                    </div>
                </div>
            </div>
            <input type="submit" name="button" class="button" value="Đăng ký" />
            </div>

    </form>
    </fieldset>
</body>

</html>